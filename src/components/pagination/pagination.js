import React from 'react'
import { useLocation } from 'react-router-dom';
import {Link} from 'react-router-dom';
import './pagination.css';
const Pager = (props) => {
    let location = useLocation();
    let pageing = '';
    console.log(props);
    if(props.totalPage !== 1 && props.currentPage === props.totalPage) {
        pageing = (
            <div className="text-center">
                <Link  onClick={() => props.pageClick(props.currentPage - 1)} to={location.pathname+'?pageNumber='+(props.currentPage - 1)}><i className="tim-icons icon-minimal-left" /></Link>
                <Link onClick={() => props.pageClick(props.currentPage)}  to={location.pathname+'?pageNumber='+(props.currentPage)} className="disabled" ><i className="tim-icons icon-minimal-right" /></Link>
            </div>
        );
    }
    if(props.totalPage !== 1 && props.currentPage < props.totalPage) {
        pageing = (
            <div className="text-center">
                <Link  onClick={() => props.pageClick(props.currentPage - 1)} to={location.pathname+'?pageNumber='+(props.currentPage - 1)}><i className="tim-icons icon-minimal-left" /></Link>
                <Link onClick={() => props.pageClick(props.currentPage + 1)} to={location.pathname+'?pageNumber='+(props.currentPage + 1)} ><i className="tim-icons icon-minimal-right" /></Link>
            </div>
        );
    }
    if(props.totalPage !== 1 && props.currentPage === 1) {
        pageing = (
            <div className="text-center">
                <Link onClick={() => props.pageClick(props.currentPage)} to={location.pathname+'?pageNumber='+(props.currentPage)} className="disabled"><i className="tim-icons icon-minimal-left" /></Link>
                <Link onClick={() => props.pageClick(props.currentPage + 1)} to={location.pathname+'?pageNumber='+(props.currentPage + 1)}><i className="tim-icons icon-minimal-right" /></Link>
            </div>
        );
    }
    return (
        <>
            {pageing}
        </>
    )
}

export default Pager;