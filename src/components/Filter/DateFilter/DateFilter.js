import React, {useState, useEffect} from 'react'
import { DateRangePicker, DatePicker } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
import './datetime.css';
export default function DateFilter(props) {
    const [startdate, setStartdate] = useState('');
    const [enddate, setEnddate] = useState('');
    const dateVal = [];
    dateVal[0] = '';
    dateVal[1] = '';
    useEffect(() => {    
        const dt = [];
        dt[0] = startdate;
        dt[1] = enddate;
        props.setDate(dt);
        
    }, [startdate, enddate])

    return (
        <div className="mr-2 d-flex">
            <div>
                <label className="date-label">Start Date</label>
                <DatePicker format="YYYY-MM-DD HH:mm:ss" onChange={value => setStartdate(value)} block appearance="subtle" />
            </div>
            <div>
            <label className="date-label">End Date</label>
                <DatePicker format="YYYY-MM-DD HH:mm:ss"  onChange={value => setEnddate(value)} block appearance="subtle" />
            </div>
            
            {/* <DateRangePicker value={date} onChange={value => props.setDate(value)} /> */}
        </div>
    )
}
