import React from 'react'

const FieldFilter = ({ onChangeField, filterList, filterText, onFilter}) => {
    return (
        <div>
            <div className="d-flex">
                <select onChange={onChangeField} name="searchOn" id="searchOn" className="form-control mr-2">
                    <option value="">Select Column</option>
                    {filterList.map((filter, index) => (<option key={index} value={filter.selector}>{filter.name}</option>))}
                </select>
                <input className="form-control dev-search-text" type="text" id="search"  placeholder="Filter By Name" value={filterText} onChange={onFilter} />    
            </div>
        </div>
    )
}
export default FieldFilter;
