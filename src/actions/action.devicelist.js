import { ADD_DEVICE, FILL_DEVICE, SELECTED_DEVICE, DEV_FILTER_VALUE, DEV_FILTER_KEY } from './actionType';
import axios from '../axios.orders';
const defaultPageSize = 5;
export const addDevice = (obj) => {
    return {
        type: ADD_DEVICE,
        value: obj
    }
}

export const fillDefaultDevice = (devices) => {
    return {
        type: FILL_DEVICE,
        value: devices
    }
}

export const setFilterKey = (key) => {
    return {
        type: DEV_FILTER_KEY,
        value: key
    }    
}

export const setFilterValue = (val) => {
    return {
        type: DEV_FILTER_VALUE,
        value: val
    }    
}
export const getDeviceList = (param = null) => {
    return dispatch => {
        let url = `devices?pageSize=${defaultPageSize}`;
        let pagination = '';
        let filter = '';
        if(param && param.page) {
            pagination = `&pageNumber=${param.page}`;
        }
        if(param.device_id && param.device_mac) {
            filter = `&device_id=${param.device_id}&device_mac=${param.device_mac}`;
        }
        if(param.device_id && !param.device_mac) {
            filter = `&device_id=${param.device_id}`;
        }
        if(!param.device_id && param.device_mac) {
            filter = `&device_mac=${param.device_mac}`;
        }
        axios.get(`${url}${pagination}${filter}`).then(res => {
            dispatch(fillDefaultDevice(res.data.data));
        })
    }
}

export const setSelectedDevice = (device) => {
    return {
        type: SELECTED_DEVICE,
        value: device
    }
}