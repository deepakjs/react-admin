import { GET_DEVICE_STATE, CLEAR_DEVICE_STATE, STATE_FILTER_KEY, STATE_FILTER_VALUE } from './actionType';
import axios from '../axios.orders';
const defaultPageSize = 5;
export const fillDeviceState = (deviceState) => {
    return {
        type: GET_DEVICE_STATE,
        value: deviceState
    }
}
export const celarDeviceState = () => {
    return {
        type: CLEAR_DEVICE_STATE,
    }
}

export const setFilterKey = (key) => {
    return {
        type: STATE_FILTER_KEY,
        value: key
    }    
}
export const setFilterValue = (val) => {
    return {
        type: STATE_FILTER_VALUE,
        value: val
    }    
}

export const getDeviceState = (obj) => {
    return dispatch => {
        let url = 'device-state?device_id='+obj.id+'&pageSize='+defaultPageSize;
        if(obj.page) {
            url = `device-state?device_id=${obj.id}&pageNumber=${obj.page}&pageSize=${defaultPageSize}`;
        }
        if(obj.startTime && obj.endTime) {
            url = `device-state?device_id=${obj.id}&pageSize=${defaultPageSize}&startTime=${obj.startTime}&endTime=${obj.endTime}`;
        }
        if(obj.logType && obj.logType !== '') {
            url = 'device-state?device_id='+obj.id+'&pageSize='+defaultPageSize+'&logType='+obj.logType;
        }
        if(obj.dateTime && obj.dateTime !== '') {
            url = 'device-state?dateTime='+obj.id+'&dateTime='+obj.dateTime;
        }

        axios.get(url).then(res => {
            dispatch(fillDeviceState(res.data));
        })
    }
}