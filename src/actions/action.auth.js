import { AUTH_TRIGGER, AUTH_LOGOUT } from './actionType';
import Amplify from '@aws-amplify/core';
import Auth from '@aws-amplify/auth';


Amplify.configure({
    Auth: {
        userPoolId: 'eu-west-2_oVIDFg8hH',
        region: 'eu-west-2',
        userPoolWebClientId: '79b14i5od1i80up9gernhe99b8',
    }
});
export const auth = (data) => {
    return dispatch => {
        try {
            Auth.signIn(data.email, data.password  ).then(res => {
                console.log(JSON.stringify(res))
                const tockenInfo = {
                    token: res.signInUserSession.idToken.jwtToken,
                    api_key: 'YJ6bUiF44L459QYNzPe0H9O7gfBKeS3994CrWCvo'
                }
                localStorage.setItem('token', JSON.stringify(tockenInfo));
                dispatch(authTrigger(tockenInfo))
            });
            
        } catch(er) {
            console.log(er)
        }
    }
}

export  const authTrigger = (token) => {
    return {
        type: AUTH_TRIGGER,
        value: JSON.stringify(token)
    }
}
export const logout = () => {
    if(localStorage.getItem('token')) {
        localStorage.removeItem('token');
    }
    return {
        type: AUTH_LOGOUT
    };
}
export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if(!token) {
            dispatch(logout())
        } else {
            dispatch(authTrigger(token))
        }
    }
}