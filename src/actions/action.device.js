import { GET_DEVICE_DETAIL, CLEAR_DEVICE, LOG_FILTER_KEY, LOG_FILTER_VALUE } from './actionType';
import axios from '../axios.orders';
const defaultPageSize = 5;
export const fillDeviceDetail = (devices) => {
    return {
        type: GET_DEVICE_DETAIL,
        value: devices
    }
}
export const celarDevice = () => {
    return {
        type: CLEAR_DEVICE,
    }
}

export const setFilterKey = (key) => {
    return {
        type: LOG_FILTER_KEY,
        value: key
    }    
}
export const setFilterValue = (val) => {
    return {
        type: LOG_FILTER_VALUE,
        value: val
    }    
}
export const getDeviceDetail = (obj) => {
    return dispatch => {
        let url = 'device-logs?device_id='+obj.id+'&pageSize='+defaultPageSize;
        if(obj.page) {
            url = `device-logs?device_id=${obj.id}&pageNumber=${obj.page}&pageSize=${defaultPageSize}`;
        }
        if(obj.startTime && obj.endTime) {
            url = `device-logs?device_id=${obj.id}&pageSize=${defaultPageSize}&startTime=${obj.startTime}&endTime=${obj.endTime}`;
        }
        if(obj.logType && obj.logType !== '') {
            url = 'device-logs?device_id='+obj.id+'&pageSize='+defaultPageSize+'&logType='+obj.logType;
        }
        if(obj.dateTime && obj.dateTime !== '') {
            url = 'device-logs?dateTime='+obj.id+'&dateTime='+obj.dateTime;
        }
        axios.get(url).then(res => {
            dispatch(fillDeviceDetail(res.data));
        })
    }
}