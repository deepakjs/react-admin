import React, { useEffect } from 'react'
import { Router, Route, Switch, Redirect, withRouter } from "react-router-dom";
import { createBrowserHistory } from "history";
import {connect} from 'react-redux';

import AdminLayout from "layouts/Admin/Admin.js";
// import RTLLayout from "layouts/RTL/RTL.js";
import Login from './views/login/login';

import * as actions from './actions/action.auth';

import "assets/scss/black-dashboard-react.scss";
import "assets/demo/demo.css";
import "assets/css/nucleo-icons.css";

const hist = createBrowserHistory();

const App = (props) => {
    props.onAuthCheckState();
    useEffect(() => {
        // props.onAuthCheckState();
    });
    let routes = (
        <Switch>
            <Route
                path={'/login'}
                component={Login}
            />
            <Redirect from="*" to="/login" />
        </Switch>
    );
    if(props.isAuthenticated && localStorage.getItem('token')) {
        routes = (
            <Switch>
                <Route path="/admin" render={props => <AdminLayout {...props} />} />
                {(hist.location.pathname && (hist.location.pathname !== '/login' && hist.location.pathname !== '/')) ? <Redirect to={hist.location.pathname} /> : <Redirect from="*" to="/admin" />}
            </Switch>
        )
    }
    return (
        <>
            <Router history={hist}>
                {(props.loading) ? routes : '' }
                
            </Router>
        </>
    );
}

const mapStateToProps = state => {
    return {
      isAuthenticated: state.auth.userToken !== null,
      loading: state.auth.loading
    };
  }
  const mapDispatchToProps = dispatch => {
    return {
        onAuthCheckState: () => dispatch(actions.authCheckState())
    }
  }

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
