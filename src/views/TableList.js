/*!

=========================================================
* Black Dashboard React v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/black-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/black-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, {useEffect} from "react";
import { connect } from "react-redux";
import { getDeviceList, setSelectedDevice, setFilterKey, setFilterValue } from '../actions/action.devicelist';
import DataTable from 'react-data-table-component';
import {Link} from 'react-router-dom';
import Pager from '../components/pagination/pagination';
import FieldFilter from '../components/Filter/FieldFilter/FieldFilter';
// reactstrap components
import {
  Card,
  CardBody,
  Row,
  Col
} from "reactstrap";


const mapStateToProps = state => {
  return { 
    deviceList: state.device.deviceList,
    requestComplete: state.device.requestComplete,
    filterKey: state.device.filterKey,
    filterValue: state.device.filterValue,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getDeviceList: (param) => dispatch(getDeviceList(param)),
    setDevice: (device) => dispatch(setSelectedDevice(device)),
    setFilterKey: (key) => dispatch(setFilterKey(key)),
    setFilterValue: (val) => dispatch(setFilterValue(val))
  }
}
const filterColumn = [
  {
    name: 'Device ID',
    selector: 'device_id',
  },
  {
    name: 'MAC address', 
    selector: 'device_mac'
  }
];
const FilterComponent = ({ filterText, onFilter, onClear, title, onChangeField }) => {
  console.log(onChangeField);
  return (
    <div className="d-flex justify-content-between align-items-center w-100">
      <div>
        <h1>{title}</h1>
      </div>
      <FieldFilter onChangeField={onChangeField} filterList={filterColumn} filterText={filterText} onFilter={onFilter} />
    </div>
  );
}
const Tables = (props) => {
  const params = new URLSearchParams(props.location.search); 
  useEffect(() => {
    const tags = params.get('pageNumber');
    const filterObj = {};
    if(tags) {
      filterObj['page'] = tags;
    }
    if(props.filterKey !== '' && props.filterValue !== '') {
      filterObj[props.filterKey] = props.filterValue;
    }
    if(!props.requestComplete) {
      props.getDeviceList(filterObj);
    }
    console.log(props.deviceList)
  }, [props.filterKey, props.filterValue]);
  
  const columns = [
    {
      name: 'Device ID',
      selector: 'device_id',
      sortable: true,
      cell: row => <Link onClick={() => props.setDevice(row)} to={`/admin/device-detail/${row.device_id}`}>{row.device_id}</Link>,
    },
    {
      name: 'MAC address', 
      selector: 'device_mac',
      sortable: true
    },
    {
      name: 'First Online',
      selector: 'inserted_at',
      sortable: true,
      cell: row => {
        const d = new Date(row.inserted_at * 1000);
        return (<span>{d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear()}</span>)
      }
    },
    {
      name: 'is online ?',
      selector: 'is_online',
      sortable: true,
      cell: row => <>{row.is_online ? 'Yes' : 'No'}</>
    },
    {
      name: 'last active',
      selector: 'last_active',
      sortable: true,
      cell: row => {
        const d = new Date(row.last_active * 1000);
        return (<span>{d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear()}</span>)
      }
    },
    {
      cell: (row) => <Link onClick={() => props.setDevice(row)} to={`/admin/device-state/${row.device_id}`}>Device State</Link>,
      ignoreRowClick: true,
      allowOverflow: true,
      button: true,
    },
  ];
 
  let data = props.deviceList.data;
  const subHeaderComponentMemo = React.useMemo(() => {
    return <FilterComponent onChangeField={e => props.setFilterKey(e.target.value)} title="Device List" onFilter={e => props.setFilterValue(e.target.value)}  filterText={props.filterValue} />;
  });

  const reloadPage = (page) => {
    console.log(page);
    props.getDeviceList({page});
  }
  return (
    <>
      <div className="content">
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <DataTable
                  title=""
                  columns={columns}
                  data={data}
                  subHeader
                  subHeaderComponent={subHeaderComponentMemo}
                />
                <Pager totalPage={props.deviceList?.totalPages} currentPage={props.deviceList?.currentPage} pageClick={(page) => reloadPage(page)} />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    </>
  );
}
const List = connect(mapStateToProps, mapDispatchToProps)(Tables);
export default List;
