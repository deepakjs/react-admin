import React, {useEffect} from 'react';
import { connect } from "react-redux";
import DataTable from 'react-data-table-component';
import Pager from '../../components/pagination/pagination';
import DateFilter from '../../components/Filter/DateFilter/DateFilter';
import FieldFilter from '../../components/Filter/FieldFilter/FieldFilter';
import { getDeviceDetail, celarDevice, setFilterKey, setFilterValue } from '../../actions/action.device';
import {
    Card,
    CardBody,
    Row,
    Col
  } from "reactstrap";
import './device-log.css';


const filterColumn = [
  {
      name: 'Log Type',
      selector: 'logType',
  },
  {
      name: 'Date Time', 
      selector: 'dateTime',
  },
];

const mapStateToProps = state => {
  return { 
    deviceDetail: state.deviceDetail.device,
    requestComplete: state.deviceDetail.requestComplete,
    selectedDevice: state.device.selectedDevice,
    filterKey: state.deviceDetail.filterKey,
    filterValue: state.deviceDetail.filterValue,
  };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getDeviceDetail: (id) => dispatch(getDeviceDetail(id)),
        celarDevice: () => dispatch(celarDevice()),
        setFilterKey: (key) => dispatch(setFilterKey(key)),
        setFilterValue: (val) => dispatch(setFilterValue(val))
    }
}
 
const FilterComponent = ({ filterText, onFilter, onClear, title, onChangeField, setDateFromFilter }) => (
  <div className="d-flex justify-content-between align-items-center w-100">
    <div>
      <h1>{title}</h1>
    </div>
    <div className="d-flex">
      <DateFilter setDate={setDateFromFilter}></DateFilter>
      {/* <FieldFilter onChangeField={onChangeField}  filterList={filterColumn} filterText={filterText} onFilter={onFilter} /> */}
    </div>
    
  </div>
);

const columns = [
  {
    name: 'Log Type',
    selector: 'log_type',
    sortable: true
  },
  {
    name: 'Date Time', 
    selector: 'dateTime',
    sortable: true,
    cell: row => {
      const d = new Date(row.logged_at * 1000);
      return (<span>{d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear()}</span>)
    }
  }
];

const Devicelog = (props) => {
    const id = props.match.params.id;
    const params = new URLSearchParams(props.location.search); 

    const setDateFromFilter = (date) => {      
      console.log(date);
      props.getDeviceDetail({id, startTime: Date.parse(date[0])/1000, endTime: Date.parse(date[1])/1000})
    }

    useEffect(() => {        
      const tags = params.get('pageNumber');
      const filterObj = {id};
      if(tags) {
        filterObj['page'] = tags;
      }
      if(!props.requestComplete) {
        props.getDeviceDetail(filterObj);
      }
      return () => {
          props.celarDevice();
      }
    }, []);
    

    const reloadPage = (page) => {
      props.getDeviceDetail({id, page});
    }

    let data = props.deviceDetail?.data.data;
    
    const SampleExpandedComponent = ({ data }) => (
      <div className="dv-detail d-flex">
        <p>
          Device automatically responds after receiving order
        </p>
        <div className="ml-4">
        {JSON.stringify(data.command)}
        </div>
      </div>
    );
    
    useEffect(() => {
      let filterObj = {id}
      if(props.filterKey !== '' && props.filterValue !== '') {
        filterObj[props.filterKey] = props.filterValue;
        props.getDeviceDetail(filterObj);
      }
    }, [props.filterKey, props.filterValue]);

    const subHeaderComponentMemo = React.useMemo(() => {
        return <FilterComponent setDateFromFilter={setDateFromFilter} onChangeField={e => props.setFilterKey(e.target.value)} title="Device List" onFilter={e => props.setFilterValue(e.target.value)} filterText={props.filterValue} />;
    });

    return (
        <div className="content dev-data">
            <div className="">
                <div className="row">
                    <div className="col-sm-9">
                        <div className="row dec-device-detail">
                            <div className="col-sm-6">
                                <label>Device ID:</label>
                                <span>{props.deviceDetail?.deviceData?.device_id}</span>
                            </div>
                            <div className="col-sm-6">
                                <label>MAC Address</label>
                                <span>{props.deviceDetail?.deviceData?.device_mac}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <Row>
                    <Col md="12">
                        <Card>
                        <CardBody>
                            <DataTable
                              title=""
                              columns={columns}
                              data={data}
                              subHeader
                              subHeaderComponent={subHeaderComponentMemo}
                              expandableRows
                              expandableRowsComponent={<SampleExpandedComponent />}
                            />
                            <Pager totalPage={props.deviceDetail?.data.totalPages} currentPage={props.deviceDetail?.data.currentPage} pageClick={(page) => reloadPage(page)}/>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        </div>
    )
}
export default connect(mapStateToProps, mapDispatchToProps)(Devicelog)