import React, {useEffect} from 'react';
import { connect } from "react-redux";
import DataTable from 'react-data-table-component';
import DateFilter from '../../components/Filter/DateFilter/DateFilter';
import FieldFilter from '../../components/Filter/FieldFilter/FieldFilter';
import Pager from '../../components/pagination/pagination';
import { setFilterKey, setFilterValue } from '../../actions/action.devicestate';
import {
    Card,
    CardBody,
    Row,
    Col
  } from "reactstrap";
import './DeviceState.css';

import { celarDeviceState, getDeviceState} from '../../actions/action.devicestate';

const filterColumn = [
    {
        name: 'State',
        selector: 'state',
    },
    {
        name: 'Date Time',
        selector: 'dateTime',
    },
];

const mapStateToProps = state => {
    return { 
        deviceState: state.deviceState.deviceState,
        requestComplete: state.deviceState.requestComplete,
        selectedDevice: state.device.selectedDevice,
        filterKey: state.deviceDetail.filterKey,
        filterValue: state.deviceDetail.filterValue,
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        getDeviceState: (id) => dispatch(getDeviceState(id)),
        celarDeviceState: () => dispatch(celarDeviceState()),
        setFilterKey: (key) => dispatch(setFilterKey(key)),
        setFilterValue: (val) => dispatch(setFilterValue(val))
    }
}

const FilterComponent = ({ filterText, onFilter, onClear, title, onChangeField, setDateFromFilter }) => (
    <div className="d-flex justify-content-between align-items-center w-100">
      <div>
        <h1>{title}</h1>
      </div>
      <div className="d-flex">
        <DateFilter setDate={setDateFromFilter}></DateFilter>
        {/* <FieldFilter onChangeField={onChangeField} filterList={filterColumn} filterText={filterText} onFilter={onFilter} /> */}
      </div>
      
    </div>
);

const columns = [
    {
        name: 'State',
        selector: 'state',
        sortable: true,
    },
    {
        name: 'Date Time', 
        selector: 'logged_at',
        sortable: true,
        cell: row => {
            const d = new Date(row.logged_at * 1000);
            return (<span>{d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear()}</span>)
        }
    }
];

const DeviceState = (props) => {
    
    const id = props.match.params.id;
    const params = new URLSearchParams(props.location.search); 

    const setDateFromFilter = (date) => {
        props.getDeviceState({id, startTime: Date.parse(date[0])/1000, endTime: Date.parse(date[1])/1000})
    }
    
    useEffect(() => {
        const tags = params.get('pageNumber');
        const filterObj = {id};
        if(tags) {
            filterObj['page'] = tags;
        }
        if(!props.requestComplete) {
          props.getDeviceState(filterObj);
        }
        return () => {
            props.celarDeviceState();
        }
    }, []);

    const reloadPage = (page) => {
        props.getDeviceState({id, page});
    }
    
    let data = props.deviceState?.data.data;

    useEffect(() => {
        let filterObj = {id}
        if(props.filterKey !== '' && props.filterValue !== '') {
          filterObj[props.filterKey] = props.filterValue;
          props.getDeviceState(filterObj);
        }
      }, [props.filterKey, props.filterValue]);

    const subHeaderComponentMemo = React.useMemo(() => {
        return <FilterComponent setDateFromFilter={setDateFromFilter} onChangeField={e => props.setFilterKey(e.target.value)} title="Device State" onFilter={e => props.setFilterValue(e.target.value)}  filterText={props.filterValue} />;
    }, [props.filterText, props.filterValue]);
    
    return (
        <div className="content dev-data">
            <div className="">
                <div className="row">
                <div className="col-sm-9">
                        <div className="row dec-device-detail">
                            <div className="col-sm-6">
                                <label>Device ID:</label>
                                <span>{props.deviceState?.deviceData?.device_id}</span>
                            </div>
                            <div className="col-sm-6">
                                <label>MAC Address</label>
                                <span>{props.deviceState?.deviceData?.device_mac}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <Row>
                    <Col md="12">
                        <Card>
                        <CardBody>
                            <DataTable
                                title=""
                                columns={columns}
                                data={data}
                                subHeader
                                subHeaderComponent={subHeaderComponentMemo}
                            />
                            <Pager totalPage={props.deviceState?.data.totalPages} currentPage={props.deviceState?.data.currentPage} pageClick={(page) => reloadPage(page)}/>
                        </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        </div>
    )
}
export default connect(mapStateToProps, mapDispatchToProps)(DeviceState);
