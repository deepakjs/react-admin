/*!

=========================================================
* Black Dashboard React v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/black-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/black-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, {useState, useEffect, useRef} from "react";
import { Card, CardBody, Row, Col } from "reactstrap";
import {useHistory} from 'react-router-dom';
import {connect} from 'react-redux';

import * as actions from '../../actions/action.auth';

import "./login.css";

const mapStateToProps = state => {
  return {
    token: state.auth.userToken
  };
}
const mapDispatchToProps = dispatch => {
  return {
      onAuth:  (authData) => dispatch(actions.auth(authData)),
  };
}
const Login = (props) => {
    document.body.classList.add("white-content");
    const [email, setEmail] = useState('');
    const firstRender = useRef(true);
    const [password, setPassword] = useState('');
    const [disable, setDisabled] = useState(true)
    const history = useHistory();
    const [error  , setError] = useState({email: null, password: null})

    useEffect(() => {
  
      if (firstRender.current) {
        firstRender.current = false
        return
      }
      if(props.token !== null) {
        history.push('/admin/list')
      }
      setDisabled(formValidation())
      
    }, [email, password])
    const validEmailRegex = 
  RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
    const formValidation = () => {
      setError({email:null, password:null});
      if (email === "") {
        error.email = 'Eemail cant be blank!'
        setError(error)
        return true
      } else if(!validEmailRegex.test(email)) {
        error.email = 'Email is invalid'
        setError(error)
        return true
      } else {
        error.email = null
        setError(error)
      }
      if(password === "") {
        error.password = 'password cant be blank!'
        setError(error);
        return true
      } else {
        error.password = null
        setError(error)
      }
      setError({email:null, password:null});
      return false;
    }

    const loginSuccess = (e) => {
        e.preventDefault();
        console.log(email);
        console.log(password);
        props.onAuth({email: email, password: password});
        // history.push('/admin/list')
    }
    return (
      <>
        <div className="content container" data="green"> 
          <Row>
            <Col md="12">
              <Card className="card-plain card-login">
                
                <CardBody>
                    <h1 className="mb-3">Login</h1>
                    <form>
                        <div className="form-group">
                            <label>Email address</label>
                            <input type="email" onChange={e => setEmail(e.target.value)} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                            { error.email && <p className="error">{error.email}</p> }
                        </div>
                        <div className="form-group">
                            <label>Password</label>
                            <input type="password" onChange={e => setPassword(e.target.value)} className="form-control" id="exampleInputPassword1" />
                            { error.password && <p className="error">{error.password}</p> }
                        </div>
                        
                        <button disabled={disable} type="submit" onClick={loginSuccess} className="btn btn-primary">Submit</button>
                    </form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
