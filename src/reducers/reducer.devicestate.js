import { GET_DEVICE_STATE, CLEAR_DEVICE_STATE, STATE_FILTER_KEY, STATE_FILTER_VALUE } from '../actions/actionType';
import { updateObject } from '../shared/factory';
const initState = {
    deviceState: null,
    requestComplete: false,
    filterKey: '',
    filterValue: ''
};

const deviceState = (state = initState, action) => {
    switch(action.type) {
        case GET_DEVICE_STATE:
            const updatedState = {
                deviceState: action.value,
                requestComplete: true,
            }
            return updateObject(state, updatedState);
        case CLEAR_DEVICE_STATE:
            return updateObject(state, {
                deviceState: null,
                requestComplete: false,
                filterKey: '',
                filterValue: ''
            });
        case STATE_FILTER_KEY:
            return updateObject(state, {
                filterKey: action.value,
                requestComplete: false,
            });
        case STATE_FILTER_VALUE:
            return updateObject(state, {
                filterValue: action.value,
                requestComplete: false,
            });
        default:
            return state;
    }
}

export default deviceState;