import { AUTH_TRIGGER, AUTH_LOGOUT } from '../actions/actionType';
import { updateObject } from '../shared/factory';
const initState = {
    userToken: null,
    loading: false
};

const auth = (state = initState, action) => {
    switch(action.type) {
        case AUTH_TRIGGER:
            return updateObject(state, {
                userToken: JSON.parse(action.value),
                loading: true
            });
        case AUTH_LOGOUT:
            return updateObject(state, {
                userToken: null,
                loading: true
            });
        default:
            return state;
    }
}

export default auth;