import { GET_DEVICE_DETAIL, CLEAR_DEVICE, LOG_FILTER_KEY, LOG_FILTER_VALUE } from '../actions/actionType';
import { updateObject } from '../shared/factory';
const initState = {
    device: null,
    requestComplete: false,
    filterKey: '',
    filterValue: ''
};

const deviceList = (state = initState, action) => {
    switch(action.type) {
        case GET_DEVICE_DETAIL:
            const updatedState = {
                device: action.value,
                requestComplete: true,
            }
            return updateObject(state, updatedState);
        case CLEAR_DEVICE:
            return updateObject(state, {
                device: null,
                requestComplete: false,
                filterKey: '',
                filterValue: ''
            });
        case LOG_FILTER_KEY:
            return updateObject(state, {
                filterKey: action.value,
                requestComplete: false,
            });
        case LOG_FILTER_VALUE:
            return updateObject(state, {
                filterValue: action.value,
                requestComplete: false,
            });
        default:
            return state;
    }
}

export default deviceList;