import { ADD_DEVICE, UPDATE_DEVICE, DELETE_DEVICE, FILL_DEVICE, SELECTED_DEVICE, REMOVE_DEVICE_SELECTION, DEV_FILTER_VALUE, DEV_FILTER_KEY } from '../actions/actionType';
import { updateObject } from '../shared/factory';
const initState = {
    deviceList: [],
    requestComplete: false,
    selectedDevice: null,
    filterKey: '',
    filterValue: ''
};

const device = (state = initState, action) => {
    switch(action.type) {
        case ADD_DEVICE:
            const updatedState = {
                deviceList: [...state.deviceList].push(action.value)
            }
            return updateObject(state, updatedState);
        case DELETE_DEVICE:
            const updatedState2 = {
                deviceList:[...state.deviceList].splice(action.value, 1)
            }
            return updateObject(state, updatedState2);
        case UPDATE_DEVICE:
            const list = [...state.deviceList];
            list[list.indexOf(action.value)] = action.value;
            return updateObject(state, {
                deviceList: list
            });
        case FILL_DEVICE:
            return updateObject(state, {
                deviceList: action.value,
                requestComplete: true,
            });
        case SELECTED_DEVICE:
            return updateObject(state, {
                selectedDevice: action.value
            });
        case REMOVE_DEVICE_SELECTION:
            return updateObject(state, {
                selectedDevice: null
            });
        case DEV_FILTER_KEY:
            return updateObject(state, {
                filterKey: action.value,
                requestComplete: false,
            });
        case DEV_FILTER_VALUE:
            return updateObject(state, {
                filterValue: action.value,
                requestComplete: false,
            });
        default:
            return state;
    }
}

export default device;