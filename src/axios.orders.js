import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://cors-anywhere.herokuapp.com/https://9ph6fe6j3d.execute-api.eu-west-2.amazonaws.com/prod/admin/'
});
instance.interceptors.request.use(request => {
    const token = JSON.parse(localStorage.getItem('token'));
    if(token !== null) {
        request.headers['x-heatzy-token'] = token.token;
        request.headers['x-api-key'] = token.api_key;
    }
    return request;
}, err => {
    console.log(err);
    return Promise.reject(err);
})
export default instance;